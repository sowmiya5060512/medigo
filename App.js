import { NavigationContainer } from '@react-navigation/native';
import AuthProvider from './src/provider/Authprovider';
import AppNavigator from './src/navigation/AppNavigator';
import { registerRootComponent } from 'expo';
import { View } from 'react-native';

export default function App() {
  return (
    <View style={{flex:1}}>
      
    <NavigationContainer>
      <AuthProvider>
        <AppNavigator/>
      </AuthProvider>
    </NavigationContainer>

    </View>
  );
}

registerRootComponent(App);


