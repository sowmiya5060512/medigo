import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HomeScreen from '../screens/HomeScreen';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import ProductScreen from '../screens/ProductScreen';
import { FontAwesome } from '@expo/vector-icons';
const Tab = createBottomTabNavigator();
const TabNavigator = () => {
    return(
        <Tab.Navigator initialRouteName="Home" >
            <Tab.Screen name="Home" component={HomeScreen}  options={{
                headerShown: false,
            tabBarIcon: () => (
              <MaterialCommunityIcons name="home" color={'#5e0a53'} size={25} />
            ),
          }} />
            <Tab.Screen name="Product" component={ProductScreen} options={{
                headerShown: false,
            tabBarIcon: () => (
                <FontAwesome name="opencart" size={25} color={"#5e0a53"} />
            ),
          }} />
           
        </Tab.Navigator>
    )
}
export default TabNavigator