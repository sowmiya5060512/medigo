import { useContext } from "react";
import { AuthContext } from "../provider/Authprovider";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import RequestOtpScreen from "../screens/RequestOtpScreen";
import VerifyOtpScreen from "../screens/VerifyOtpScreen";
import TabNavigator from "./TabNavigator";
import Header from "../components/Header";

const AppNavigator = () => {
  const Stack = createStackNavigator();
  const { user } = useContext(AuthContext);

  return (
    <NavigationContainer independent={true} initialRouteName="RequestOtp">
      <Stack.Navigator>
        {user ? (
          <Stack.Screen
            name="TabNavigator"
            component={TabNavigator}
            options={{
              header: () => <Header />,
              headerShown: true,
            }}
          />
        ) : (
          <>
            <Stack.Screen
              name="RequestOtp"
              component={RequestOtpScreen}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="VerifyOtp"
              component={VerifyOtpScreen}
              options={{ headerShown: false }}
            />
          </>
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default AppNavigator;
