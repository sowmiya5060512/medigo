import React, { createContext, useEffect, useState } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const AuthContext = createContext();

const AuthProvider = ({ children }) => {
  const [user, setUser] = useState(null);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const checkAuthentication = async () => {
      try {
        const userData = await AsyncStorage.getItem('user');
        setUser(userData ? JSON.parse(userData) : null);
      } catch (error) {
        console.error('Error checking authentication:', error);
      } finally {
        setIsLoading(false);
      }
    };

    checkAuthentication();
  }, []);

  const signIn = async (username) => {
    const userData = username;
    await AsyncStorage.setItem('user', JSON.stringify(userData));

    setUser(userData);
  };

  const signOut = async () => {
    await AsyncStorage.removeItem('user');
    setUser(null);
  };

  if (isLoading) {
    return null; 
  }

  return (
    <AuthContext.Provider value={{ user, signIn, signOut }}>
      {user ? children : <>{children}</>}
    </AuthContext.Provider>
  );
};

export default AuthProvider;
