import React, { useContext, useState } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import logo from '../asset/logo.png';
import userLogo from '../asset/usericon.png';
import { AuthContext } from '../provider/Authprovider'
import { useNavigation } from '@react-navigation/native';
const Header = () => {
    const { signOut } = useContext(AuthContext);
  const navigation = useNavigation()
    const toggleMenu = () => {
        signOut();

        navigation.navigate('RequestOtp');
    };
  
  return (
    <View style={styles.header}>
      <View style={styles.leftContainer}>
        <Image source={logo} style={styles.logo} />
        <Text style={styles.headerText}>MediGo</Text>
      </View>
      <TouchableOpacity onPress={()=>toggleMenu()} style={styles.userIconContainer}>
      <Image source={userLogo} style={styles.userLogo} />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#f8f8f8',
    padding: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    height: 60,
    display:'flex',
    flexDirection:'row',
    marginTop:30,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  leftContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  logo: {
    width: 50,
    height: 50,
    marginRight: 10,
  },
  headerText: {
    fontSize: 20,
    fontWeight: 'bold',
    fontFamily:'Roboto'
  },
  userLogo: {
    width: 30,
    height: 30,
    borderRadius: 20,
  },
  userIconContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    position: 'relative',
  },
});

export default Header;
