import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
// import LinearGradient from 'react-native-linear-gradient';
import { Entypo } from '@expo/vector-icons';
const OfferBox = () => {
  return (
    <View style={styles.container}>
      {/* <LinearGradient
        colors={['#7c5f8a', '#7d2574']}
        style={styles.gradient}
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 1 }}
      > */}
      <View>
        <Text style={styles.text}>Get 25% off on All Medicines</Text>
        <Text style={{height:20, width:70,textAlign:'center', borderRadius:10,justifyContent:'flex-end', backgroundColor:'white'}}>Buy Now</Text>
        </View>
        <Entypo name="price-tag" size={50} color="white" />
      {/* </LinearGradient> */}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding:10,
    backgroundColor:"#9e729a",
    height: 100,
    width: '95%',
    borderRadius: 10,
    overflow: 'hidden',
    display:'flex',
    flexDirection:'row',
    alignSelf:'center'
  },
  gradient: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
    textTransform: 'uppercase',
  },
});

export default OfferBox;
