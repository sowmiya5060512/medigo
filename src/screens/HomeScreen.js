import React, { useEffect, useState } from "react";
import { FlatList, Image, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View } from "react-native";
import { useBottomTabBarHeight } from '@react-navigation/bottom-tabs';
import logo from '../asset/logo.png';
import { FontAwesome5 } from '@expo/vector-icons';
import OfferBox from "../components/OfferBox";
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { FontAwesome6 } from '@expo/vector-icons';
import { Entypo } from '@expo/vector-icons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import staticData from '../utils/products.json';


function HomeScreen({ navigation }) {
    const tabBarheight = useBottomTabBarHeight();
    const [token, setToken] = useState("")
    const [allProducts, setAllproducts] = useState([]);
    const [initial, setInitial] = useState(10);
    const [page, setPage] = useState(0);

    const staticProducts = staticData.data.products

    const fetchProducts = async (tkn, currentpage) => {
        console.log("-called")
        const configAxios = axios.create({
            baseURL: 'https://devapp.lifepharmacy.com/api/',
            headers: {
                Authorization: `Bearer ${tkn}`,
            },
        });
        await configAxios.post(`products?skip=${initial * currentpage}&take=${initial}`).then(
            (res) => {
                if (res.status === 200) {
                    // navigation.navigate('App');
                    setAllproducts(res?.data?.products)

                    // console.log("---->", res);
                }
            },
            (err) => {
                console.log("error");
            }
        );
    }

    useEffect(() => {
        const checkAuthentication = async () => {
            try {
                const userData = await AsyncStorage.getItem('user');
                const value = JSON.parse(userData)
                // console.log("------------->", value?.username?.token)
                setToken(value?.username?.token)
                fetchProducts(value?.username?.token, page)
            } catch (error) {
                console.error('Error checking authentication:', error);
            } 
        };

        checkAuthentication();
    }, []);

    const renderItem = ({ item }) => (
            <View style={styles.item}>
        <TouchableOpacity onPress={()=> navigation.navigate('Product', { id: item.id })}>

                <Text style={{ fontSize: 14 }}>{item.title}</Text>
                <Text style={{ color: "grey", fontSize: 10 }}>110 mg</Text>
                {item?.images &&
                    <Image
                        //  style={styles.image}
                        source={{ uri: item?.images.logo }} // Replace the URL with your image URL
                    />
                }
        </TouchableOpacity>

            </View>

    );
    const loadMoreData = () => {
        console.log("-----------")
        setPage(page + 1)
        fetchProducts(token, page + 1)

    }
    return (
        <View style={{ height: '100%' }}>
            <View style={styles.container}>
                <FontAwesome5 name="search" size={20} color="purple" />
                <TextInput
                    style={styles.input}
                    placeholder="Search..."
                // value={searchText}
                // onChangeText={setSearchText}
                />
            </View>
            <OfferBox />
            <View style={{ padding: 10 }}>
                <Text style={{ marginBottom: 5,fontSize:16, fontWeight:'bold'  }}>Top Categories</Text>
                <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
                    <View style={styles.box}>
                        <Text style={{ fontSize: 17, color: '#5c0650' }}>Wellness
                        </Text>
                        <MaterialCommunityIcons name="leaf" size={24} color={"#a61192"} />
                    </View>
                    <View style={styles.box}><Text style={{ fontSize: 17, color: '#5c0650' }}>Homeo
                    </Text>
                        <FontAwesome6 name="house-medical-circle-check" size={24} color={"#a61192"} /></View>
                    <View style={styles.box}><Text style={{ fontSize: 17, color: '#5c0650' }}>Eyecare
                    </Text>
                        <Entypo name="eye" size={24} color={"#a61192"} />
                    </View>
                </View>
            </View>
            <Text style={{ padding: 10, fontSize:16, fontWeight:'bold' }}>Products</Text>
            <View style={{ flex: 1 }}>
                <FlatList
                    data={allProducts?.length > 0 ? allProducts : staticProducts}
                    numColumns={2}
                    renderItem={renderItem}
                    keyExtractor={(item) => item.id}
                    contentContainerStyle={styles.flatListContainer}
                    onEndReached={loadMoreData}
                    onEndReachedThreshold={0}
                />

            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#f0f0f0',
        paddingHorizontal: 10,
        borderRadius: 5,
        marginHorizontal: 10,
        marginVertical: 10,
        borderColor: 'grey',
        elevation: 3
    },
    icon: {
        marginRight: 10,
    },
    input: {
        flex: 1,
        height: 40,
    },
    box: {
        display: 'flex',
        width: 100,
        height: 70,
        backgroundColor: "#d6c7d9",
        borderRadius: 20,
        padding: 10,
        alignItems: 'center',
        elevation: 5
    },
    flatListContainer: {
        paddingHorizontal: 16,
        paddingTop: 16,
    },
    item: {
        flex: 1,
        margin: 8,
        alignItems: 'center',
        justifyContent: 'center',
        height: 100,
        backgroundColor: '#d8e9f0',
        // borderWidth: 1,
        // borderColor: 'none',
        borderRadius: 15,
        elevation:1
    },

})
export default HomeScreen
