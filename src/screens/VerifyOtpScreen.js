import React, { useContext, useRef, useState } from "react";
import { Image, StyleSheet, Text, TextInput, TouchableOpacity, View } from "react-native";
import logo from '../asset/logo.png';
import { AuthContext } from "../provider/Authprovider";
import configAxios from "../utils/axiosConfig";
import { useNavigation } from "@react-navigation/native";

function VerifyOtpScreen() {
    const { signIn } = useContext(AuthContext);
    const [otp, setOtp] = useState(['', '', '', '']);
    const otpInputRefs = useRef([]);
    const navigation = useNavigation()

    const handleSubmit = () => {
        if(otp){
            const payload = {
                phone: "971526050392",
                code: 3486,
            }
            configAxios.post("auth/verify-otp",payload ).then((res) => {
                if (res.status === 200) {
                    signIn(res.data.data)
                    navigation.navigate('TabNavigator')
                    console.log(res.data.data)
                }
            },
                (err) => {
                    console.log("error")
                });
        }
    }

    const handleChangeText = (text, index) => {
        let newOtp = [...otp];
        newOtp[index] = text;
        setOtp(newOtp);

        if (text !== '' && index < otp.length - 1) {
            otpInputRefs.current[index + 1].focus();
        }
    };

    const handleKeyPress = ({ nativeEvent, preventDefault }) => {
        if (nativeEvent.key === 'Backspace') {
            const currentIndex = otpInputRefs.current.findIndex(input => input.isFocused());

            if (currentIndex > 0) {
                let newOtp = [...otp];
                newOtp[currentIndex - 1] = '';
                setOtp(newOtp);
                otpInputRefs.current[currentIndex - 1].focus();
            }
        }
    };

    return (
        <View style={styles.requestContainer}>
            <View style={styles.card}>
                <Image source={logo} style={styles.logo} />
                <Text style={styles.title}>Enter OTP</Text>
                <View style={styles.otpContainer}>
                    {otp.map((digit, index) => (
                        <TextInput
                            key={index}
                            ref={(ref) => (otpInputRefs.current[index] = ref)}
                            style={styles.otpInput}
                            value={digit}
                            onChangeText={(text) => handleChangeText(text, index)}
                            onKeyPress={handleKeyPress}
                            maxLength={1}
                            keyboardType="numeric"
                            autoFocus={index === 0}
                        />
                    ))}
                </View>
                <TouchableOpacity style={styles.button} onPress={handleSubmit}>
                    <Text style={styles.buttonText}>Submit</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    requestContainer: {
        display: "flex",
        alignItems: "center",
        backgroundColor: '#e8e6e6',
        justifyContent: 'center',
        height: "100%",
        width: "100%"
    },
    card: {
        padding: 5,
        width: 340,
        height: 500,
        backgroundColor: "white",
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        display: 'flex',
        flexDirection: 'column',

        alignContent: 'center',
        alignItems: 'center'
    },
    logo: {
        width: 200,
        height: 200,
    },
    button: {
        backgroundColor: '#387047',
        borderRadius: 60,
        paddingHorizontal: 20,
        paddingVertical: 10,
        width: 240
    },
    title: {
        fontSize: 20,
        marginBottom: 20,
    },
    buttonText: {
        color: 'white',
        fontSize: 16,
        textAlign: 'center',
    },
    otpContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginBottom: 20,
    },
    otpInput: {
        width: 40,
        height: 40,
        borderWidth: 1,
        borderColor: '#ccc',
        borderRadius: 5,
        marginHorizontal: 5,
        textAlign: 'center',
        fontSize: 18,
    },
});


export default VerifyOtpScreen

