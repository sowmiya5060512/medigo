import React, { useState } from "react";
import { TextInput, Image, StyleSheet, Text, TouchableOpacity, View, Alert } from "react-native";
import logo from '../asset/logo.png';
import configAxios from "../utils/axiosConfig";

function RequestOtpScreen({navigation}) {
    const [mobilenumber, setMobilenUmber] = useState("")
    const [name, setName] = useState("")
    const handleSubmit = () => {
        console.log("hi")
        if(mobilenumber && name){
            const payload = {
                phone: "971526205290",
                name: "test name"
            }
            configAxios.post("auth/request-otp",payload ).then((res) => {
                if (res.status === 200) {
                    Alert.alert(`OTP sent Successfully!`,"check at your inbox", [
                        {
                          text: 'Cancel',
                          onPress: () => navigation.navigate('VerifyOtp'),
                          style: 'cancel',
                        },
                        {text: 'OK', onPress: () => navigation.navigate('VerifyOtp')},
                      ]);
                  
                    // navigation.navigate('VerifyOtp');
                    console.log(res.data)
                }
            },
                (err) => {
                    console.log("error")
                });
        }
    }
    return (
        <View style={styles.requestContainer}>
            <View style={styles.card}>
                <Image source={logo} style={styles.logo} />
                <TextInput
                    placeholder="Mobile Number"
                    keyboardType="phone-pad"
                    maxLength={10}
                    style={styles.inputbox} value={mobilenumber} onChangeText={(text) => setMobilenUmber(text)} />
                <TextInput
                    placeholder="Name"
                    style={styles.inputbox} value={name} onChangeText={(text) => setName(text)} />
                <TouchableOpacity style={styles.button} onPress={handleSubmit}>
                    <Text style={styles.buttonText}>Submit</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    requestContainer: {
        display: "flex",
        alignItems: "center",
        backgroundColor: '#e8e6e6',
        justifyContent: 'center',
        height: "100%",
        width: "100%"
    },
    card: {
        padding: 5,
        width: 340,
        height: 500,
        backgroundColor: "white",
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        display: 'flex',
        flexDirection: 'column',

        alignContent: 'center',
        alignItems: 'center'
    },
    logo: {
        width: 200,
        height: 200,
    },
    inputbox: {
        padding: 10,
        borderWidth: 1,
        borderRadius: 60,
        fontSize: 16,
        margin: 5,
        alignSelf: 'center',
        width: 240,
        height: 40,
        borderColor: "grey",
        marginBottom: 10
    },
    head: {
        display: 'flex',
        textAlign: 'left',
        color: 'grey',
        fontWeight: 'bold'
    },
    button: {
        backgroundColor: '#387047',
        borderRadius: 60,
        paddingHorizontal: 20,
        paddingVertical: 10,
        width: 240
    },
    buttonText: {
        color: 'white',
        fontSize: 16,
        textAlign: 'center',
    },
});


export default RequestOtpScreen

