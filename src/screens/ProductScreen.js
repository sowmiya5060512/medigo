import React, { useEffect, useState } from "react";
import { Image, ScrollView, StyleSheet, Text, View } from "react-native";
import { useBottomTabBarHeight } from '@react-navigation/bottom-tabs';
import logo from '../asset/logo.png';
import configAxios from "../utils/axiosConfig";
import { Entypo } from '@expo/vector-icons';

function ProductScreen({ route }) {
    const tabBarheight = useBottomTabBarHeight();
    const [data, setData] = useState(null);
    const [name, setName] = useState("Fitness Bull");
    const [image, setImage] = useState("https://life-cdn.lifepharmacy.com/brand-logos/herbatint.png")
    const [id, setId] = useState("")
    useEffect(() => {
        if(id){
        configAxios.get(`products/${id}`).then((res) => {
            if (res.status === 200) {
                setData(res.data)
                console.log("----------->",res.data.data.product_details.brand.name)
                setName(res.data.data.product_details.brand.name)
                setImage(res.data.data.product_details.brand.images.logo)
            }
        },
            (err) => {
                console.log("error")
            });
        }
    }, [id])
    useEffect(()=>{
    if (route?.params){
        let param = route?.params;
        setId(param.id)

    }
    },[])
    return (
        <View style={{padding:20, alignItems:'center'}}>
            <Text style={{fontWeight:'bold', fontSize:20}}>Brand Name: {name}</Text>
            <Image
                         style={styles.image}
                        source={{ uri: image }} 
                    />
           <View style={{display:'flex', flexDirection:'row'}}>
           <Entypo name="location-pin" size={24} color="black" />
            <Text>Deliver within 3-4 days</Text>
            <Text style={{color:'#1c5f7a'}}>12 Rich Terrace, wigaram</Text>
           </View>
           <View>
            <Text style={{fontWeight:'bold', fontSize:16}}>Overview</Text>
            <Text>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla at condimentum
             a eleifend ligula vulputate. Mauris tincidunt augue sed enim aliquam,
              id vestibulum lectus pharetra. Duis ac bibendum elit. Proin vel dui ligula.
               Duis vestibulum ligula ut tincidunt feugiat. Quisque nec nibh ultricies,
                viverra elit eget, ultricies nisl. Nam eget nulla eu tellus condimentum 
            </Text>
            <Text style={{fontWeight:'bold', fontSize:16}}>How to use </Text>
            <Text>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla at condimentum
             a eleifend ligula vulputate. Mauris tincidunt augue sed enim aliquam,
              id vestibulum lectus pharetraies nisl. Nam eget nulla eu tellus condimentum 
            </Text>
           </View>
        </View>
    )
}
const styles = StyleSheet.create({
image: {
    width: 250,
    height: 250,
    marginBottom: 10,
    resizeMode: 'stretch',
  },})
export default ProductScreen