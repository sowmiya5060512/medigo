import axios from 'axios';

const configAxios = axios.create({
  baseURL: 'https://devapp.lifepharmacy.com/api/',
  timeout: 5000, // 5 seconds
  headers: {
    'Content-Type': 'application/json',
  },
});

// Add a request interceptor
configAxios.interceptors.request.use(
  function (config) {
    // const token = 'your-authentication-token';
    
    // if (token) {
    //   config.headers.Authorization = `Bearer ${token}`;
    // }
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

// Add a response interceptor
configAxios.interceptors.response.use(
  function (response) {
    // You can modify the response data here
    return response;
  },
  function (error) {
    return Promise.reject(error);
  }
);

export default configAxios;